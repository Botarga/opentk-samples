﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Input;

namespace ConsoleApp6
{
    class MultipleLights : GameWindow
    {
        private ShaderProgram mainProgram;
        private ShaderProgram lightProgram;
        Texture diffuseMap;
        Texture specularMap;

        private int VAO, VBO;
        private int LightVAO;
        Vector3 lightPos = new Vector3(1.2f, 1.0f, 2.0f);

        // camera
        private Camera camera;
        Vector2 lastMousePos = new Vector2();

        float[] vertices = new float[]{
           // positions          // normals           // texture coords
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f,  1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f,  0.0f,

        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  1.0f,  1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f,

        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
        -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
        -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
        -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  1.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
         0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  1.0f,
         0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  1.0f,  0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f,  0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f,  1.0f,

        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f,
         0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  1.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f,  1.0f
        };
        Vector3[] cubePositions = {
            new Vector3( 0.0f,  0.0f,  0.0f),
            new Vector3( 2.0f,  5.0f, -15.0f),
            new Vector3(-1.5f, -2.2f, -2.5f),
            new Vector3(-3.8f, -2.0f, -12.3f),
            new Vector3( 2.4f, -0.4f, -3.5f),
            new Vector3(-1.7f,  3.0f, -7.5f),
            new Vector3( 1.3f, -2.0f, -2.5f),
            new Vector3( 1.5f,  2.0f, -2.5f),
            new Vector3( 1.5f,  0.2f, -1.5f),
            new Vector3(-1.3f,  1.0f, -1.5f)
        };
        // positions of the point lights
        Vector3[] pointLightPositions = {
            new Vector3( 0.7f,  0.2f,  2.0f),
            new Vector3( 2.3f, -3.3f, -4.0f),
            new Vector3(-4.0f,  2.0f, -12.0f),
            new Vector3( 0.0f,  0.0f, -3.0f)
        };

        public MultipleLights(int width, int height) : base(width, height) { }

        protected override void OnLoad(EventArgs e)
        {
            mainProgram = new ShaderProgram(
                new List<ShaderInfo> {
                new ShaderInfo { ShaderPath = Program.SHADERS_PATH + "MultipleLightsVert.glsl", ShaderType = ShaderType.VertexShader},
                new ShaderInfo { ShaderPath = Program.SHADERS_PATH + "MultipleLightsFrag.glsl", ShaderType = ShaderType.FragmentShader}
                }
            );
            lightProgram = new ShaderProgram(
                new List<ShaderInfo> {
                new ShaderInfo { ShaderPath = Program.SHADERS_PATH + "MultipleLightsLampVert.glsl", ShaderType = ShaderType.VertexShader},
                new ShaderInfo { ShaderPath = Program.SHADERS_PATH + "MultipleLightsLampFrag.glsl", ShaderType = ShaderType.FragmentShader}
                }
            );

            camera = new Camera();
            lastMousePos = new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
            CursorVisible = false;

            GL.ClearColor(0.1f, 0.1f, 0.1f, 1.0f);
            VAO = GL.GenVertexArray();
            VBO = GL.GenBuffer();
            GL.BindVertexArray(VAO);
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, VAO);
                GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StaticDraw);
                GL.EnableVertexAttribArray(0);
                GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 8 * sizeof(float), 0);
                GL.EnableVertexAttribArray(1);
                GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, 8 * sizeof(float), 3 * sizeof(float));
                GL.EnableVertexAttribArray(2);
                GL.VertexAttribPointer(2, 2, VertexAttribPointerType.Float, false, 8 * sizeof(float), 6 * sizeof(float));
            }
            GL.BindVertexArray(0);
            LightVAO = GL.GenVertexArray();
            GL.BindVertexArray(LightVAO);
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
                GL.EnableVertexAttribArray(0);
                GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 8 * sizeof(float), 0);
            }
            GL.BindVertexArray(0);
            diffuseMap = new Texture(Program.TEXTURES_PATH + "container.png");
            specularMap = new Texture(Program.TEXTURES_PATH + "containerSpecular.png");

            mainProgram.Use();
            mainProgram.SetInt("material.diffuse", 0);
            mainProgram.SetInt("material.specular", 1);

            GL.Enable(EnableCap.DepthTest);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            mainProgram.Use();
            mainProgram.SetVector3("viewPos", camera.Position);
            mainProgram.SetFloat("material.shininess", 32.0f);

            mainProgram.SetVector3("light.position", camera.Position);
            mainProgram.SetVector3("light.direction", camera.Front);
            mainProgram.SetFloat("light.cutOff", (float)Math.Cos(MathHelper.DegreesToRadians(12.5f)));
            mainProgram.SetFloat("light.outerCutOff", (float)Math.Cos(MathHelper.DegreesToRadians(17.5f)));


            // light properties
            // directional light
            mainProgram.SetVector3("dirLight.direction", -0.2f, -1.0f, -0.3f);
            mainProgram.SetVector3("dirLight.ambient", 0.05f, 0.05f, 0.05f);
            mainProgram.SetVector3("dirLight.diffuse", 0.4f, 0.4f, 0.4f);
            mainProgram.SetVector3("dirLight.specular", 0.5f, 0.5f, 0.5f);
            // point light 1
            mainProgram.SetVector3("pointLights[0].position", pointLightPositions[0]);
            mainProgram.SetVector3("pointLights[0].ambient", 0.05f, 0.05f, 0.05f);
            mainProgram.SetVector3("pointLights[0].diffuse", 0.8f, 0.8f, 0.8f);
            mainProgram.SetVector3("pointLights[0].specular", 1.0f, 1.0f, 1.0f);
            mainProgram.SetFloat("pointLights[0].constant", 1.0f);
            mainProgram.SetFloat("pointLights[0].linear", 0.09f);
            mainProgram.SetFloat("pointLights[0].quadratic", 0.032f);
            // point light 2
            mainProgram.SetVector3("pointLights[1].position", pointLightPositions[1]);
            mainProgram.SetVector3("pointLights[1].ambient", 0.05f, 0.05f, 0.05f);
            mainProgram.SetVector3("pointLights[1].diffuse", 0.8f, 0.8f, 0.8f);
            mainProgram.SetVector3("pointLights[1].specular", 1.0f, 1.0f, 1.0f);
            mainProgram.SetFloat("pointLights[1].constant", 1.0f);
            mainProgram.SetFloat("pointLights[1].linear", 0.09f);
            mainProgram.SetFloat("pointLights[1].quadratic", 0.032f);
            // point light 3
            mainProgram.SetVector3("pointLights[2].position", pointLightPositions[2]);
            mainProgram.SetVector3("pointLights[2].ambient", 0.05f, 0.05f, 0.05f);
            mainProgram.SetVector3("pointLights[2].diffuse", 0.8f, 0.8f, 0.8f);
            mainProgram.SetVector3("pointLights[2].specular", 1.0f, 1.0f, 1.0f);
            mainProgram.SetFloat("pointLights[2].constant", 1.0f);
            mainProgram.SetFloat("pointLights[2].linear", 0.09f);
            mainProgram.SetFloat("pointLights[2].quadratic", 0.032f);
            // point light 4
            mainProgram.SetVector3("pointLights[3].position", pointLightPositions[3]);
            mainProgram.SetVector3("pointLights[3].ambient", 0.05f, 0.05f, 0.05f);
            mainProgram.SetVector3("pointLights[3].diffuse", 0.8f, 0.8f, 0.8f);
            mainProgram.SetVector3("pointLights[3].specular", 1.0f, 1.0f, 1.0f);
            mainProgram.SetFloat("pointLights[3].constant", 1.0f);
            mainProgram.SetFloat("pointLights[3].linear", 0.09f);
            mainProgram.SetFloat("pointLights[3].quadratic", 0.032f);
            // spotLight
            mainProgram.SetVector3("spotLight.position", camera.Position);
            mainProgram.SetVector3("spotLight.direction", camera.Front);
            mainProgram.SetVector3("spotLight.ambient", 0.0f, 0.0f, 0.0f);
            mainProgram.SetVector3("spotLight.diffuse", 1.0f, 1.0f, 1.0f);
            mainProgram.SetVector3("spotLight.specular", 1.0f, 1.0f, 1.0f);
            mainProgram.SetFloat("spotLight.constant", 1.0f);
            mainProgram.SetFloat("spotLight.linear", 0.09f);
            mainProgram.SetFloat("spotLight.quadratic", 0.032f);
            mainProgram.SetFloat("spotLight.cutOff", (float)Math.Cos(MathHelper.DegreesToRadians(12.5f))); 
            mainProgram.SetFloat("spotLight.outerCutOff", (float)Math.Cos(MathHelper.DegreesToRadians(15.0f)));


            // view/projection transformations
            Matrix4 view = camera.GetViewMatrix();
            Matrix4 projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(45.0f), Width / Height, 0.1f, 100.0f);
            mainProgram.SetMat4("projection", projection);
            mainProgram.SetMat4("view", view);

            // world transformation
            Matrix4 model = Matrix4.Identity;
            mainProgram.SetMat4("model", model);


            // bind diffuse map
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, diffuseMap.Id);
            // bind specular map
            GL.ActiveTexture(TextureUnit.Texture1);
            GL.BindTexture(TextureTarget.Texture2D, specularMap.Id);

            // render the cube
            GL.BindVertexArray(VAO);
            for (int i = 0; i < 10; i++)
            {
                float angle = 20.0f * i;
                // calculate the model matrix for each object and pass it to shader before drawing
                model = Matrix4.Identity *
                    Matrix4.CreateFromAxisAngle(new Vector3(1.0f, 0.3f, 0.5f), MathHelper.DegreesToRadians(angle)) *
                    Matrix4.CreateTranslation(cubePositions[i]);
                mainProgram.SetMat4("model", model);

                GL.DrawArrays(PrimitiveType.Triangles, 0, 36);
            }

            // also draw the lamp object(s)
            lightProgram.Use();
            lightProgram.SetMat4("projection", projection);
            lightProgram.SetMat4("view", view);

            // we now draw as many light bulbs as we have point lights.
            GL.BindVertexArray(LightVAO);
            for (int i = 0; i < 4; i++)
            {
                model = Matrix4.Identity *
                    Matrix4.CreateScale(0.2f) *
                    Matrix4.CreateTranslation(pointLightPositions[i]);
                lightProgram.SetMat4("model", model);
                GL.DrawArrays(PrimitiveType.Triangles, 0, 36);
            }

            SwapBuffers();
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);
            ProcessInput();
        }

        private void ProcessInput()
        {
            if (Focused)
            {
                Vector2 delta = lastMousePos - new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
                lastMousePos += delta;

                camera.AddRotation(delta.X, delta.Y);
                lastMousePos = new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
            }

            if (Keyboard.GetState().IsKeyDown(Key.W))
            {
                camera.Move(0f, 0.1f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.S))
            {
                camera.Move(0f, -0.1f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.A))
            {
                camera.Move(-0.1f, 0f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.D))
            {
                camera.Move(0.1f, 0f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.Q))
            {
                camera.Move(0f, 0f, 0.1f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.E))
            {
                camera.Move(0f, 0f, -0.1f);
            }
            if (Keyboard.GetState().IsKeyDown(Key.I))
                lightPos.Z -= 0.1f;
            if (Keyboard.GetState().IsKeyDown(Key.K))
                lightPos.Z += 0.1f;
            if (Keyboard.GetState().IsKeyDown(Key.J))
                lightPos.X -= 0.1f;
            if (Keyboard.GetState().IsKeyDown(Key.L))
                lightPos.X += 0.1f;
            if (Keyboard.GetState().IsKeyDown(Key.U))
                lightPos.Y += 0.1f;
            if (Keyboard.GetState().IsKeyDown(Key.O))
                lightPos.Y -= 0.1f;

            if (Keyboard.GetState().IsKeyDown(Key.Escape))
            {
                Exit();
            }
        }

        protected override void OnFocusedChanged(EventArgs e)
        {
            base.OnFocusedChanged(e);
            lastMousePos = new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
        }


        protected override void OnResize(EventArgs e)
        {
            GL.Viewport(0, 0, Width, Height);
        }
    }
}