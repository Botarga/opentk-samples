﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{

    class Program
    {
        public const string SHADERS_PATH = @"..\..\shaderFiles\";
        public const string TEXTURES_PATH = @"..\..\Textures\";
        public const string MODELS_PATH = @"..\..\Models\";
        public const double FRAMERATE = 60.0;
        public const int WIDTH = 1000, HEIGHT = 750;
        public static Stopwatch stopWatch = new Stopwatch();

        private static void ShowBasicsMenu()
        {
            bool exit = false;
            while (!exit)
            {
                stopWatch.Reset();
                stopWatch.Start();
                Console.Clear();
                Console.WriteLine("Computer Graphics Basics Menu");
                Console.WriteLine("1.  Clear canvas");
                Console.WriteLine("2.  Draw a triangle");
                Console.WriteLine("3.  Draw a square with EBO Buffer");
                Console.WriteLine("4.  Draw a texture.");
                Console.WriteLine("5.  Draw mixed texture.");
                Console.WriteLine("6.  Transformations I rotating a texture.");
                Console.WriteLine("7.  Transformations II perspective with a texture.");
                Console.WriteLine("8.  Draw a cube.");
                Console.WriteLine("9.  Draw more cubes.");
                Console.WriteLine("10. FPS like Camera.");
                Console.WriteLine("e. Back");
                var option = Console.ReadLine();
                switch (option)
                {
                    case "1":
                        using (var ex = new DrawCanvas(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;

                    case "2":
                        using (var ex = new DrawTriangle(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;

                    case "3":
                        using (var ex = new DrawTriangleEBOBuffer(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;

                    case "4":
                        using (var ex = new DrawTexture(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;

                    case "5":
                        using (var ex = new DrawMixedTextures(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;

                    case "6":
                        using (var ex = new TransformationsIRotatingTexture(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;

                    case "7":
                        using (var ex = new TransformationsIIPerspectiveTexture(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;

                    case "8":
                        using (var ex = new DrawACube(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;

                    case "9":
                        using (var ex = new DrawMoreCubes(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;

                    case "10":
                        using (var ex = new FPSLikeCamera(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;

                    case "e":
                        exit = true;
                        break;
                }
            }
        }

        private static void ShowLoadingModelMenu()
        {
            bool exit = false;
            while (!exit)
            {
                stopWatch.Reset();
                stopWatch.Start();
                Console.Clear();
                Console.WriteLine("LOADING MODEL MENU");
                Console.WriteLine("1.  Nanosuit Model");
                //Console.WriteLine("2.  BB8 textures not loading ");
                Console.WriteLine("e. Back");
                var option = Console.ReadLine();
                switch (option)
                {
                    case "1":
                        using (var ex = new LoadModelExercise(WIDTH, HEIGHT, "nanosuit"))
                            ex.Run(FRAMERATE);
                        break;

                    //case "2":
                    //    using (var ex = new LoadModelExercise(WIDTH, HEIGHT, "bb8"))
                    //        ex.Run(FRAMERATE);
                    //    break;

                    case "e":
                        exit = true;
                        break;
                }
            }
        }

        private static void ShowLightningEffectsMenu()
        {
            bool exit = false;
            while (!exit)
            {
                stopWatch.Reset();
                stopWatch.Start();
                Console.Clear();
                Console.WriteLine("LIGHTNING EFFECTS MENU");
                Console.WriteLine("1. Simple absolute light.");
                Console.WriteLine("2. Phong Lightning");
                Console.WriteLine("3. Materials I");
                Console.WriteLine("4. Materials II (in progress)");
                Console.WriteLine("5. Lightning maps with specular map");
                Console.WriteLine("6. Light Casters Directional Light");
                Console.WriteLine("7. Light Casters Point Light");
                Console.WriteLine("8. Light Casters Spot Light");
                Console.WriteLine("9. Multiple Lights");
                Console.WriteLine("e. Back");
                var option = Console.ReadLine();

                switch (option)
                {
                    case "1":
                        using (var ex = new SimpleAbsoluteLight(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;

                    case "2":
                        using (var ex = new PhongLightning(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;

                    case "3":
                        using (var ex = new MaterialsLightning(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;

                    case "4":
                        using (var ex = new MaterialsLightningII(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;

                    case "5":
                        using (var ex = new LightningMapsWithSpecularMap(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;

                    case "6":
                        using (var ex = new LightCasterDirectionalLight(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;

                    case "7":
                        using (var ex = new LightCasterPointLight(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;

                    case "8":
                        using (var ex = new LightCasterSpotLight(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;

                    case "9":
                        using (var ex = new MultipleLights(WIDTH, HEIGHT))
                            ex.Run(FRAMERATE);
                        break;
                    case "e":
                        exit = true;
                        break;
                }
            }
        }

        private static void ShowAdvancedGraphicsMenu()
        {
            bool exit = false;
            while (!exit)
            {
                stopWatch.Reset();
                stopWatch.Start();
                Console.Clear();
                Console.WriteLine("SHOW ADVANCED GRAPHICS MENU");
                Console.WriteLine("1. Stencil test");
                
                Console.WriteLine("e. Back");
                var option = Console.ReadLine();
                switch (option)
                {
                    case "1":
                        using (var ex = new LoadModelExercise(WIDTH, HEIGHT, "nanosuit"))
                            ex.Run(FRAMERATE);
                        break;

                    //case "2":
                    //    using (var ex = new LoadModelExercise(WIDTH, HEIGHT, "bb8"))
                    //        ex.Run(FRAMERATE);
                    //    break;

                    case "e":
                        exit = true;
                        break;
                }
            }
        }

        private static void ShowMainMenu()
        {
            bool exit = false;

            while (!exit)
            {
                Console.Clear();
                Console.WriteLine("MAIN MENU");
                Console.WriteLine("1. Computer graphics Basics");
                Console.WriteLine("2. Lightning Effects");
                Console.WriteLine("3. Loading Models");
                Console.WriteLine("4. Advanced Graphics (in progress)");
                Console.WriteLine("5. Advanced Lightning (in progress)");
                Console.WriteLine("6. PBR (in progress)");
                Console.WriteLine("e. Exit");
                var option = Console.ReadLine();
                switch (option)
                {
                    case "1":
                        ShowBasicsMenu();
                        break;

                    case "2":
                        ShowLightningEffectsMenu();
                        break;

                    case "3":
                        ShowLoadingModelMenu();
                        break;

                    case "e":
                        exit = true;
                        break;
                }
            }
        }

        static void Main(string[] args)
        {
            ShowMainMenu();
        }
    }
}
