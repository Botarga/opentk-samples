﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using System.IO;

namespace ConsoleApp6
{
    public class ShaderProgram
    {
        private int ProgramID;
        
        public ShaderProgram(List<ShaderInfo> shaders)
        {
            ProgramID = GL.CreateProgram();
            int param;
            var shaderObjects = new List<int>();

            foreach(var sh in shaders)
            {
                if (File.Exists(sh.ShaderPath))
                {
                    string shaderSource = File.ReadAllText(sh.ShaderPath);
                    int shaderObject = GL.CreateShader(sh.ShaderType);
                    GL.ShaderSource(shaderObject, shaderSource);
                    GL.CompileShader(shaderObject);
                    GL.GetShader(shaderObject, ShaderParameter.CompileStatus, out param);
                    if (param == 0)
                        Console.WriteLine(String.Format("{0}::COMPILATION::STATUS: {1}", sh.ShaderType.ToString(), GL.GetShaderInfoLog(shaderObject)));
                    else
                    {
                        GL.AttachShader(ProgramID, shaderObject);
                        shaderObjects.Add(shaderObject);
                    }
                }
                else
                    Console.WriteLine("Error leyendo shader de ruta " + sh.ShaderPath);
            }

            GL.LinkProgram(ProgramID);
            GL.GetProgram(ProgramID, GetProgramParameterName.LinkStatus, out param);
            if (param == 0)
                Console.WriteLine("PROGRAM::LINK::STATUS: " + GL.GetProgramInfoLog(ProgramID));

            shaderObjects.ForEach(so => { GL.DetachShader(ProgramID, so); GL.DeleteShader(so); });
        }

        public void Use()
        {
            GL.UseProgram(ProgramID);
        }

        public void Delete()
        {
            GL.DeleteProgram(ProgramID);
        }

        public void SetInt(string location, int value)
        {
            GL.Uniform1(GL.GetUniformLocation(ProgramID, location), value);
        }

        public void SetMat4(string location, Matrix4 matrix)
        {
            GL.UniformMatrix4(GL.GetUniformLocation(ProgramID, location), false, ref matrix);
        }

        public void SetVector3(string location, Vector3 vector)
        {
            GL.Uniform3(GL.GetUniformLocation(ProgramID, location), vector);
        }

        public void SetFloat(string location, float value)
        {
            GL.Uniform1(GL.GetUniformLocation(ProgramID, location), value);
        }

        public void SetVector3(string location, float x, float y, float z)
        {
            GL.Uniform3(GL.GetUniformLocation(ProgramID, location), new Vector3(x, y, z));
        }
    }
    public class ShaderInfo
    {
        public ShaderType ShaderType { get; set; }
        public string ShaderPath { get; set; }
    }
}
