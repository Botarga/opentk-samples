﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace ConsoleApp6
{
    class TransformationsIRotatingTexture : GameWindow
    {
        private ShaderProgram mainProgram;
        private int VAO, VBO, EBO;
        Texture waterTexture, rockTexture;

        private float[] vertices = new float[] {
            -0.5f,  0.5f, 0.0f,   0.0f, 0.0f,
             0.5f,  0.5f, 0.0f,   1.0f, 0.0f,
            -0.5f, -0.5f, 0.0f,   0.0f, 1.0f,
             0.5f, -0.5f, 0.0f,   1.0f, 1.0f
        };

        private int[] indices = new int[]{
            0, 1, 2,
            1, 2, 3
        };

        public TransformationsIRotatingTexture(int width, int height) : base(width, height) { }

        protected override void OnLoad(EventArgs e)
        {
            mainProgram = new ShaderProgram(
                new List<ShaderInfo> {
                new ShaderInfo { ShaderPath = Program.SHADERS_PATH + "TransformationsIRotatingTextureVert.glsl", ShaderType = ShaderType.VertexShader},
                new ShaderInfo { ShaderPath = Program.SHADERS_PATH + "TransformationsIRotatingTextureFrag.glsl", ShaderType = ShaderType.FragmentShader}
                }
            );

            GL.ClearColor(0.3f, 0.5f, 0.7f, 1.0f);
            VAO = GL.GenVertexArray();
            VBO = GL.GenBuffer();
            EBO = GL.GenBuffer();
            GL.BindVertexArray(VAO);
            {
                GL.BindBuffer(BufferTarget.ArrayBuffer, VAO);
                GL.BufferData(BufferTarget.ArrayBuffer, vertices.Length * sizeof(float), vertices, BufferUsageHint.StaticDraw);
                GL.EnableVertexAttribArray(0);
                GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 5 * sizeof(float), 0);
                GL.EnableVertexAttribArray(1);
                GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, 5 * sizeof(float), 3 * sizeof(float));

                GL.BindBuffer(BufferTarget.ElementArrayBuffer, EBO);
                GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Length * sizeof(int), indices, BufferUsageHint.StaticDraw);
            }
            GL.BindVertexArray(0);

            waterTexture = new Texture(Program.TEXTURES_PATH + "water.jpg");
            rockTexture = new Texture(Program.TEXTURES_PATH + "rocks.jpg");
            mainProgram.Use();
            mainProgram.SetInt("texture1", 0);
            mainProgram.SetInt("texture2", 1);
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, waterTexture.Id);
            GL.ActiveTexture(TextureUnit.Texture1);
            GL.BindTexture(TextureTarget.Texture2D, rockTexture.Id);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);

            Matrix4 transform = 
                Matrix4.CreateRotationZ(Program.stopWatch.ElapsedMilliseconds * 0.005f) * 
                Matrix4.CreateTranslation(new Vector3(0.5f, -0.5f, 0.0f));
            mainProgram.SetMat4("transform", transform);
            
            GL.BindVertexArray(VAO);
            GL.DrawElements(BeginMode.Triangles, indices.Length, DrawElementsType.UnsignedInt, 0);
            SwapBuffers();
        }

        protected override void OnResize(EventArgs e)
        {
            GL.Viewport(0, 0, Width, Height);
        }
    }
}
