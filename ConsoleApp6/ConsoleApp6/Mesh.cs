﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL4;
using System.Runtime.InteropServices;
namespace ConsoleApp6
{
    public struct Vertex
    {
        public Vector3 Position;
        public Vector3 Normal;
        public Vector2 TexCoords;
        public Vector3 Tangent;
        public Vector3 Bitangent;
    };

    public class Mesh
    {
        public List<Vertex> vertices;
        public List<int> indices;
        public List<Texture> textures;
        int VAO;
        private int VBO, EBO;
        public static int sizeOfVertex;

        public Mesh(List<Vertex> vertices, List<int> indices, List<Texture> textures)
        {
            this.vertices = vertices;
            this.indices = indices;
            this.textures = textures;
            Vertex sample = new Vertex();
            sizeOfVertex = Marshal.SizeOf(sample);
            SetupMesh();
        }

        public void Draw(ShaderProgram shader)
        {
            int diffuseNr = 1;
            int specularNr = 1;
            int normalNr = 1;
            int heightNr = 1;
            for (int i = 0; i < textures.Count; i++)
            {
                GL.ActiveTexture(TextureUnit.Texture0 + i);

                string number = "";
                string name = textures[i].Type;
                switch (name)
                {
                    case "texture_diffuse":
                        number = Convert.ToString(diffuseNr++);
                        break;
                    case "texture_specular":
                        number = Convert.ToString(specularNr++);
                        break;
                    case "texture_normal":
                        number = Convert.ToString(normalNr++);
                        break;
                    case "texture_height":
                        number = Convert.ToString(heightNr++);
                        break;
                }

                shader.SetInt((name + number), i);
                GL.BindTexture(TextureTarget.Texture2D, textures[i].Id);
            }

            GL.BindVertexArray(VAO);
            GL.DrawElements(BeginMode.Triangles, indices.Count, DrawElementsType.UnsignedInt, 0);
            GL.BindVertexArray(0);

            GL.ActiveTexture(TextureUnit.Texture0);
        }

        void SetupMesh()
        {
            VAO = GL.GenVertexArray();
            VBO = GL.GenBuffer();
            EBO = GL.GenBuffer();

            GL.BindVertexArray(VAO);
            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);

            GL.BufferData(BufferTarget.ArrayBuffer, vertices.Count * sizeOfVertex,  vertices.ToArray(), BufferUsageHint.StaticDraw);

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, EBO);
            GL.BufferData(BufferTarget.ElementArrayBuffer, indices.Count * sizeof(int), indices.ToArray(), BufferUsageHint.StaticDraw);

            GL.EnableVertexAttribArray(0);
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, sizeOfVertex, 0);
            // vertex normals
            GL.EnableVertexAttribArray(1);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, sizeOfVertex, Marshal.OffsetOf(typeof(Vertex), "Normal"));
            // vertex texture coords
            GL.EnableVertexAttribArray(2);
            GL.VertexAttribPointer(2, 2, VertexAttribPointerType.Float, false, sizeOfVertex, Marshal.OffsetOf(typeof(Vertex), "TexCoords"));
            // vertex tangent
            GL.EnableVertexAttribArray(3);
            GL.VertexAttribPointer(3, 3, VertexAttribPointerType.Float, false, sizeOfVertex, Marshal.OffsetOf(typeof(Vertex), "Tangent"));
            // vertex bitangent
            GL.EnableVertexAttribArray(4);
            GL.VertexAttribPointer(4, 3, VertexAttribPointerType.Float, false, sizeOfVertex, Marshal.OffsetOf(typeof(Vertex), "Bitangent"));

            GL.BindVertexArray(0);
        }

    }
}
