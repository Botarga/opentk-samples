﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using OpenTK.Input;

namespace ConsoleApp6
{
    class LoadModelExercise : GameWindow
    {
        private ShaderProgram mainProgram;
        private Camera camera;
        Model myModel;
        Vector2 lastMousePos = new Vector2();
        public LoadModelExercise(int width, int height) : base(width, height) { }
        bool linesMode = false;
        string modelName;
        public LoadModelExercise(int width, int height, string model) : base(width, height)
        {
            modelName = model;
        }

        protected override void OnLoad(EventArgs e)
        {
            mainProgram = new ShaderProgram(
                new List<ShaderInfo> {
                new ShaderInfo { ShaderPath = Program.SHADERS_PATH + "ModelLoadingVert.glsl", ShaderType = ShaderType.VertexShader},
                new ShaderInfo { ShaderPath = Program.SHADERS_PATH + "ModelLoadingFrag.glsl", ShaderType = ShaderType.FragmentShader}
                }
            );
            camera = new Camera();
            lastMousePos = new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
            CursorVisible = false;
            if(modelName == "nanosuit")
                myModel = new Model(Program.MODELS_PATH + @"nanosuit\nanosuit.obj");
            else if(modelName == "bb8")
                myModel = new Model(Program.MODELS_PATH + @"BB8\bb8.obj");
            GL.ClearColor(0.3f, 0.5f, 0.7f, 1.0f);
            GL.Enable(EnableCap.DepthTest);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            Matrix4 model = Matrix4.Identity;

            mainProgram.Use();
            Matrix4 view = camera.GetViewMatrix();
            Matrix4 projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(45.0f), Width / Height, 0.1f, 100.0f);
            mainProgram.SetMat4("view", view);
            mainProgram.SetMat4("projection", projection);


            model = Matrix4.Identity * Matrix4.CreateTranslation(0.0f, -1.75f, 0.0f);
            mainProgram.SetMat4("model", model);
            myModel.Draw(mainProgram);

            SwapBuffers();
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);
            ProcessInput();
        }

        private void ProcessInput()
        {
            if (Focused)
            {
                Vector2 delta = lastMousePos - new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
                lastMousePos += delta;

                camera.AddRotation(delta.X, delta.Y);
                lastMousePos = new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
            }

            if (Keyboard.GetState().IsKeyDown(Key.W))
            {
                camera.Move(0f, 0.1f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.S))
            {
                camera.Move(0f, -0.1f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.A))
            {
                camera.Move(-0.1f, 0f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.D))
            {
                camera.Move(0.1f, 0f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.Q))
            {
                camera.Move(0f, 0f, 0.1f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.E))
            {
                camera.Move(0f, 0f, -0.1f);
            }
            if (Keyboard.GetState().IsKeyDown(Key.M))
            {
                if (linesMode)
                    GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);
                else
                    GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
                linesMode = !linesMode;
            }
            if (Keyboard.GetState().IsKeyDown(Key.Escape))
            {
                Exit();
            }
        }

        protected override void OnFocusedChanged(EventArgs e)
        {
            base.OnFocusedChanged(e);
            lastMousePos = new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
        }

        protected override void OnResize(EventArgs e)
        {
            GL.Viewport(0, 0, Width, Height);
        }
    }
}
