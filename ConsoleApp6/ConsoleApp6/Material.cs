﻿using System.Collections.Generic;
using OpenTK;

namespace ConsoleApp6
{
    class Material
    {
        public enum MaterialName{
            Emerald, Jade, Obsidian, Pearl, Ruby, Turqouise, Brass, Bronze, Chrome, Copper, Gold, Silver,
            BlackPlastic, CyanPlastic, GreenPlastic, RedPlastic, WhitePlastic, BlackRubber, CyanRubber,
            GreenRubber, RedRubber, WhiteRubber, YellowRubber
        }

        public Vector3 Ambient { get; set; }
        public Vector3 Diffuse { get; set; }
        public Vector3 Specular { get; set; }
        public float Shininess { get; set; }

        public static Dictionary<MaterialName, Material> materials = new Dictionary<MaterialName, Material> {
            {
                MaterialName.Emerald,
                new Material{
                    Ambient = new Vector3(0.0215f, 0.1745f, 0.0215f),
                    Diffuse = new Vector3(0.07568f, 0.61424f, 0.07568f),
                    Specular = new Vector3(0.633f, 0.727811f, 0.633f),
                    Shininess = 0.6f
                }
            },
            {
                MaterialName.Jade,
                new Material{
                    Ambient = new Vector3(0.135f, 0.2225f, 0.1575f),
                    Diffuse = new Vector3(0.54f, 0.89f, 0.63f),
                    Specular = new Vector3(0.316228f, 0.316228f, 0.316228f),
                    Shininess = 0.1f
                }
            },
            {
                MaterialName.Obsidian,
                new Material{
                    Ambient = new Vector3(0.05375f, 0.05f, 0.06625f),
                    Diffuse = new Vector3(0.18275f, 0.17f, 0.22525f),
                    Specular = new Vector3(0.332741f, 0.328634f, 0.346435f),
                    Shininess = 0.3f
                }
            },
            {
                MaterialName.Pearl,
                new Material{
                    Ambient = new Vector3(0.25f, 0.20725f, 0.20725f),
                    Diffuse = new Vector3(1.0f, 0.829f, 0.829f),
                    Specular = new Vector3(0.296648f, 0.296648f, 0.296648f),
                    Shininess = 0.088f
                }
            },
            {
                MaterialName.Ruby,
                new Material{
                    Ambient = new Vector3(0.1745f, 0.01175f, 0.01175f),
                    Diffuse = new Vector3(0.61424f, 0.04136f, 0.04136f),
                    Specular = new Vector3(0.7278111f, 0.626959f, 0.626959f),
                    Shininess = 0.06f
                }
            },
            {
                MaterialName.Turqouise,
                new Material{
                    Ambient = new Vector3(0.1f, 0.18725f, 0.1745f),
                    Diffuse = new Vector3(0.396f, 0.74151f, 0.69102f),
                    Specular = new Vector3(0.297254f, 0.30829f, 0.306678f),
                    Shininess = 0.1f
                }
            },
            {
                MaterialName.Brass,
                new Material{
                    Ambient = new Vector3(0.329412f, 0.223529f, 0.027451f),
                    Diffuse = new Vector3(0.780392f, 0.568627f, 0.113725f),
                    Specular = new Vector3(0.992157f, 0.941176f, 0.807843f),
                    Shininess = 0.21794872f
                }
            },
            {
                MaterialName.Bronze,
                new Material{
                    Ambient = new Vector3(0.2125f, 0.1275f, 0.054f),
                    Diffuse = new Vector3(0.714f, 0.4284f, 0.18144f),
                    Specular = new Vector3(0.393548f, 0.271906f, 0.166721f),
                    Shininess = 0.2f
                }
            },
            //{
            //    //chrome	0.25	0.25	0.25	0.4	0.4	0.4	0.774597	0.774597	0.774597	0.6
            //    MaterialName.,
            //    new Material{
            //        Ambient = new Vector3(0.f, 0.f, 0.f),
            //        Diffuse = new Vector3(0.f, 0.f, 0.f),
            //        Specular = new Vector3(0.f, 0.f, 0.f),
            //        Shininess = 0.f
            //    }
            //},
            //{
            //    //copper	0.19125	0.0735	0.0225	0.7038	0.27048	0.0828	0.256777	0.137622	0.086014	0.1
            //    MaterialName.,
            //    new Material{
            //        Ambient = new Vector3(0.f, 0.f, 0.f),
            //        Diffuse = new Vector3(0.f, 0.f, 0.f),
            //        Specular = new Vector3(0.f, 0.f, 0.f),
            //        Shininess = 0.f
            //    }
            //},
            //{
            //    //gold	0.24725	0.1995	0.0745	0.75164	0.60648	0.22648	0.628281	0.555802	0.366065	0.4
            //    MaterialName.,
            //    new Material{
            //        Ambient = new Vector3(0.f, 0.f, 0.f),
            //        Diffuse = new Vector3(0.f, 0.f, 0.f),
            //        Specular = new Vector3(0.f, 0.f, 0.f),
            //        Shininess = 0.f
            //    }
            //},
            //{
            //    //silver	0.19225	0.19225	0.19225	0.50754	0.50754	0.50754	0.508273	0.508273	0.508273	0.4
            //    MaterialName.,
            //    new Material{
            //        Ambient = new Vector3(0.f, 0.f, 0.f),
            //        Diffuse = new Vector3(0.f, 0.f, 0.f),
            //        Specular = new Vector3(0.f, 0.f, 0.f),
            //        Shininess = 0.f
            //    }
            //},
            //{
            //    //black plastic   0.0	0.0	0.0	0.01	0.01	0.01	0.50	0.50	0.50	.25
            //    MaterialName.,
            //    new Material{
            //        Ambient = new Vector3(0.f, 0.f, 0.f),
            //        Diffuse = new Vector3(0.f, 0.f, 0.f),
            //        Specular = new Vector3(0.f, 0.f, 0.f),
            //        Shininess = 0.f
            //    }
            //},
            //{
            //    //cyan plastic    0.0	0.1	0.06	0.0	0.50980392	0.50980392	0.50196078	0.50196078	0.50196078	.25
            //    MaterialName.,
            //    new Material{
            //        Ambient = new Vector3(0.f, 0.f, 0.f),
            //        Diffuse = new Vector3(0.f, 0.f, 0.f),
            //        Specular = new Vector3(0.f, 0.f, 0.f),
            //        Shininess = 0.f
            //    }
            //},
            //{
            //    //green plastic   0.0	0.0	0.0	0.1	0.35	0.1	0.45	0.55	0.45	.25
            //    MaterialName.,
            //    new Material{
            //        Ambient = new Vector3(0.f, 0.f, 0.f),
            //        Diffuse = new Vector3(0.f, 0.f, 0.f),
            //        Specular = new Vector3(0.f, 0.f, 0.f),
            //        Shininess = 0.f
            //    }
            //},
            //{
            //    //red plastic 0.0	0.0	0.0	0.5	0.0	0.0	0.7	0.6	0.6	.25
            //    MaterialName.,
            //    new Material{
            //        Ambient = new Vector3(0.f, 0.f, 0.f),
            //        Diffuse = new Vector3(0.f, 0.f, 0.f),
            //        Specular = new Vector3(0.f, 0.f, 0.f),
            //        Shininess = 0.f
            //    }
            //},
            //{
            //    //white plastic   0.0	0.0	0.0	0.55	0.55	0.55	0.70	0.70	0.70	.25
            //    MaterialName.,
            //    new Material{
            //        Ambient = new Vector3(0.f, 0.f, 0.f),
            //        Diffuse = new Vector3(0.f, 0.f, 0.f),
            //        Specular = new Vector3(0.f, 0.f, 0.f),
            //        Shininess = 0.f
            //    }
            //},
            //{
            //    //yellow plastic  0.0	0.0	0.0	0.5	0.5	0.0	0.60	0.60	0.50	.25
            //    MaterialName.,
            //    new Material{
            //        Ambient = new Vector3(0.f, 0.f, 0.f),
            //        Diffuse = new Vector3(0.f, 0.f, 0.f),
            //        Specular = new Vector3(0.f, 0.f, 0.f),
            //        Shininess = 0.f
            //    }
            //},
            //{
            //    //black rubber    0.02	0.02	0.02	0.01	0.01	0.01	0.4	0.4	0.4	.078125
            //    MaterialName.,
            //    new Material{
            //        Ambient = new Vector3(0.f, 0.f, 0.f),
            //        Diffuse = new Vector3(0.f, 0.f, 0.f),
            //        Specular = new Vector3(0.f, 0.f, 0.f),
            //        Shininess = 0.f
            //    }
            //},
            //{
            //    //cyan rubber 0.0	0.05	0.05	0.4	0.5	0.5	0.04	0.7	0.7	.078125
            //    MaterialName.,
            //    new Material{
            //        Ambient = new Vector3(0.f, 0.f, 0.f),
            //        Diffuse = new Vector3(0.f, 0.f, 0.f),
            //        Specular = new Vector3(0.f, 0.f, 0.f),
            //        Shininess = 0.f
            //    }
            //},
            //{
            //    //green rubber    0.0	0.05	0.0	0.4	0.5	0.4	0.04	0.7	0.04	.078125
            //    MaterialName.,
            //    new Material{
            //        Ambient = new Vector3(0.f, 0.f, 0.f),
            //        Diffuse = new Vector3(0.f, 0.f, 0.f),
            //        Specular = new Vector3(0.f, 0.f, 0.f),
            //        Shininess = 0.f
            //    }
            //},
            //{
            //    //red rubber  0.05	0.0	0.0	0.5	0.4	0.4	0.7	0.04	0.04	.078125
            //    MaterialName.,
            //    new Material{
            //        Ambient = new Vector3(0.f, 0.f, 0.f),
            //        Diffuse = new Vector3(0.f, 0.f, 0.f),
            //        Specular = new Vector3(0.f, 0.f, 0.f),
            //        Shininess = 0.f
            //    }
            //},
            //{
            //    //white rubber    0.05	0.05	0.05	0.5	0.5	0.5	0.7	0.7	0.7	.078125
            //    MaterialName.,
            //    new Material{
            //        Ambient = new Vector3(0.f, 0.f, 0.f),
            //        Diffuse = new Vector3(0.f, 0.f, 0.f),
            //        Specular = new Vector3(0.f, 0.f, 0.f),
            //        Shininess = 0.f
            //    }
            //},
            //{
            //    //yellow rubber   0.05	0.05	0.0	0.5	0.5	0.4	0.7	0.7	0.04	.078125
            //    MaterialName.,
            //    new Material{
            //        Ambient = new Vector3(0.f, 0.f, 0.f),
            //        Diffuse = new Vector3(0.f, 0.f, 0.f),
            //        Specular = new Vector3(0.f, 0.f, 0.f),
            //        Shininess = 0.f
            //    }
            //}
        };

        public static Material GetMaterialFromName(MaterialName name)
        {
            if (materials.ContainsKey(name))
                return materials[name];
            return null;
        }
    }
}
