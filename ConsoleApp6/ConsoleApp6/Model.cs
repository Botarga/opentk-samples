﻿using System;
using System.Collections.Generic;
using Assimp;
using OpenTK;

namespace ConsoleApp6
{
    public class Model
    {
        List<Texture> textures_loaded = new List<Texture>();
        List<Mesh> meshes = new List<Mesh>();
        string directory;
        bool gammaCorrection;

        public Model(string path, bool gamma = false)
        {
            gammaCorrection = gamma;
            LoadModel(path);
        }

        // draws the model, and thus all its meshes
        public void Draw(ShaderProgram shader)
        {
            for (int i = 0; i < meshes.Count; i++)
                meshes[i].Draw(shader);
        }

        private void LoadModel(string path)
        {
            AssimpContext importer = new AssimpContext();
            Scene scene = importer.ImportFile(path, PostProcessSteps.FlipUVs | PostProcessSteps.Triangulate | PostProcessSteps.CalculateTangentSpace);
            if (scene == null || ((scene.SceneFlags & SceneFlags.Incomplete) != 0) || scene.RootNode == null)
            {
                Console.WriteLine("ERROR::ASSIMP:: " + path);
                return;
            }
            directory = path.Substring(0, path.LastIndexOf('\\') + 1);

            // process ASSIMP's root node recursively
            ProcessNode(scene.RootNode, ref scene);
        }

        // processes a node in a recursive fashion.
        void ProcessNode(Node node, ref Scene scene)
        {
            // process meshes at current node
            for (int i = 0; i < node.MeshCount; i++)
            {
                Assimp.Mesh mesh = scene.Meshes[node.MeshIndices[i]];
                meshes.Add(ProcessMesh(ref mesh, ref scene));
            }
            for (int i = 0; i < node.ChildCount; i++)
            {
                ProcessNode(node.Children[i], ref scene);
            }

        }

        Mesh ProcessMesh(ref Assimp.Mesh mesh, ref Scene scene)
        {
            // data to fill
            List<Vertex> vertices = new List<Vertex>();
            List<int> indices = new List<int>();
            List<Texture> textures = new List<Texture>();

            for (int i = 0, j = mesh.Vertices.Count; i < j; i++)
            {
                Vertex vertex = new Vertex();

                vertex.Position.X = mesh.Vertices[i].X;
                vertex.Position.Y = mesh.Vertices[i].Y;
                vertex.Position.Z = mesh.Vertices[i].Z;

                // normals
                if (mesh.Normals.Count > 0) { 
                    vertex.Normal.X = mesh.Normals[i].X;
                    vertex.Normal.Y = mesh.Normals[i].Y;
                    vertex.Normal.Z = mesh.Normals[i].Z;
                }
                // texture coordinates
                if (mesh.HasTextureCoords(0)) // does the mesh contain texture coordinates?
                {
                    vertex.TexCoords.X = mesh.TextureCoordinateChannels[0][i].X;
                    vertex.TexCoords.Y = mesh.TextureCoordinateChannels[0][i].Y;
                }
                else
                    vertex.TexCoords = Vector2.Zero;

                // tangent
                if (mesh.Tangents.Count > 0)
                {
                    vertex.Tangent.X = mesh.Tangents[i].X;
                    vertex.Tangent.Y = mesh.Tangents[i].Y;
                    vertex.Tangent.Z = mesh.Tangents[i].Z;
                }
                // bitangent
                if (mesh.BiTangents.Count > 0)
                {
                    vertex.Bitangent.X = mesh.BiTangents[i].X;
                    vertex.Bitangent.Y = mesh.BiTangents[i].Y;
                    vertex.Bitangent.Z = mesh.BiTangents[i].Z;
                }
                vertices.Add(vertex);
            }
            for (int i = 0, k = mesh.FaceCount; i < k; i++)
            {
                Assimp.Face face = mesh.Faces[i];
                // retrieve all indices of the face and store them in the indices vector
                for (int j = 0; j < face.IndexCount; j++)
                    indices.Add(face.Indices[j]);
            }
            // process materials
            Assimp.Material material = scene.Materials[mesh.MaterialIndex];
            
            // 1. diffuse maps
            List<Texture> diffuseMaps = LoadMaterialTextures(ref material, Assimp.TextureType.Diffuse, "texture_diffuse");
            textures.AddRange(diffuseMaps);
            // 2. specular maps
            List<Texture> specularMaps = LoadMaterialTextures(ref material, Assimp.TextureType.Specular, "texture_specular");
            textures.AddRange(specularMaps);
            // 3. normal maps
            List<Texture> normalMaps = LoadMaterialTextures(ref material, Assimp.TextureType.Height, "texture_normal");
            textures.AddRange(normalMaps);
            // 4. height maps
            List<Texture> heightMaps = LoadMaterialTextures(ref material, Assimp.TextureType.Ambient, "texture_height");
            textures.AddRange(heightMaps);

            return new Mesh(vertices, indices, textures);
        }

        private List<Texture> LoadMaterialTextures(ref Assimp.Material mat, Assimp.TextureType type, string typeName)
        {
            List<Texture> textures = new List<Texture>();
            for (int i = 0, k = mat.GetMaterialTextures(type).Length; i < k; i++)
            {
                TextureSlot str;
                mat.GetMaterialTexture(type, i, out str);

                bool skip = false;
                for (int j = 0; j < textures_loaded.Count; j++)
                {
                    if (textures_loaded[j].Path == str.FilePath)
                    {
                        textures.Add(textures_loaded[j]);
                        skip = true; 
                        break;
                    }
                }
                if (!skip)
                {   
                    Texture texture = new Texture(str.FilePath, directory);
                    texture.Type = typeName;
                    texture.Path = str.FilePath;
                    textures.Add(texture);
                    textures_loaded.Add(texture);  
                }
            }
            return textures;
        }

    }
}
