#version 430 core

layout (location = 0) in vec3 aVertex;
layout (location = 1) in vec3 aColor;
out vec4 vertexColor;

void main()
{
	gl_Position = vec4(aVertex, 1.0);
	vertexColor = vec4(aColor, 1.0);
}
