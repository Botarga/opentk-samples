﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace ConsoleApp6
{
    public class DrawCanvas : GameWindow
    {
        public DrawCanvas(int width, int height) : base(width, height)
        {}

        protected override void OnLoad(EventArgs e)
        {
            GL.ClearColor(0.3f, 0.4f, 0.6f, 1.0f);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit);
            SwapBuffers();
        }

        protected override void OnResize(EventArgs e)
        {
            GL.Viewport(0, 0, Width, Height);
        }
    }
}
